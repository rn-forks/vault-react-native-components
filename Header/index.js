import React from 'react';
import PropTypes from 'prop-types';
import { View, StatusBar } from 'react-native';
import style from './style';

/**
* You can use this component in order to show whatever you want at the top of the screen.
*/
const Header = (props) => {
  const { children } = props;

  if (!children) return null;

  // TODO: Implement StatusBar props.
  return (
    <View style={style.container}>
      <StatusBar />
      {children}
    </View>
  );
};

Header.propTypes = {
  /** The components that you need to show in the header */
  children: PropTypes.node.isRequired,
};

export default Header;
