import { StyleSheet } from 'react-native';
import { STATUS_BAR_HEIGHT, HEADER_HEIGHT } from '../utils/constants';

const style = StyleSheet.create({
  // Main container
  container: {
    height: HEADER_HEIGHT,
    paddingTop: STATUS_BAR_HEIGHT,
  },
});

export default style;
