import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Button,
  Modal,
  StyleSheet,
  Platform,
  TouchableOpacity
} from 'react-native';
import ItemSelector from '../ItemSelector';

/**
* Component used to show a list of items inside a modal visible at the bottom of the screen.
*/
class ItemSelectorModal extends Component {

  constructor(props, context) {
    super(props, context);

    this.closeModal = this.closeModal.bind(this);
    this.openModal = this.openModal.bind(this);

    this.state = {
      modalVisible: false,
    };
  }

  getItemSelectorProps() {
    const props = {};
    const { onItemSelect, title } = this.props;
    const SELECTOR_TITLE = title;

    props.title = (Platform.OS === 'ios' && title) ? SELECTOR_TITLE.toUpperCase() : SELECTOR_TITLE;
    props.dataSource = this.getSelectorDataSource();
    props.onItemSelect = onItemSelect;
    props.overFullScreen = true;
    props.onPressCancelButton = this.closeModal;

    return props;
  }

  getSelectorDataSource() {
    const { items } = this.props;
    return items
  }

  setModalVisible(visible) {
    this.setState({
      modalVisible: visible,
    })
  }

  openModal() {
    this.setState({ modalVisible: true });
  }

  closeModal() {
    this.setState({ modalVisible: false });
  }

  render() {
    return (
      <View style={[styles.container, this.props.containerStyle]}>
        <Modal
          visible={this.state.modalVisible}
          animationType={'fade'}
          onRequestClose={this.closeModal}
          animated
          transparent
        >
          <TouchableOpacity
            style={[styles.modalContainer, this.props.modalContainerStyle]}
            activeOpacity={1}
            onPressOut={() => {this.setModalVisible(false)}}
          >
            <View style={[styles.innerContainer, this.props.innerContainerStyle]}>
              <ItemSelector {...this.getItemSelectorProps()} />
            </View>
          </TouchableOpacity>
        </Modal>
        <Button
          onPress={this.openModal}
          title="Open modal"
        />
      </View>
    );
  }
}

/*
* Implementation Example

onItineraryItemSelect(item) {
    const { tripDetail, navigator } = this.props;
    const tripId = tripDetail && tripDetail.id;
    const itemType = item && item.type;

    if (navigator) {
      navigator.push({
        addItineraryItem: true,
        tripId,
        itemType,
      });
    }
  }

  render() {
    return (
      <ItemSelectorModal
        onItemSelect={this.onItineraryItemSelect}
        items={[
          {
            type: 'flight',
            name: 'Vuelos',
          },
          {
            type: 'hotel',
            name: 'Hoteles',
          },
        ]}
        title={'Titulo pasado por props'} //optional
      />
    );
  }
*/

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: '#00000040',
  },
  innerContainer: {
    ...Platform.select({
      ios: {
      },
      android: {
        backgroundColor: 'white',
      },
    }),
  },
});

ItemSelectorModal.propTypes = {
  /** Callback that is called when an item is selected. Parameter: 'item' selected. */
  onItemSelect: PropTypes.func.isRequired,
  /** A items collection */
  items: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
    })
  ),
  /** The modal title */
  title: PropTypes.string,
};

export default ItemSelectorModal;
