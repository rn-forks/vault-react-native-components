import { NativeModules, Platform } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';


const getAndroidStatusBarHeight = () => {
  return (Platform.Version > 21) ? 0 : StatusBarManager.HEIGHT;
};

// Spacing with status bar.
const { StatusBarManager } = NativeModules;
const STATUS_BAR_HEIGHT = (Platform.OS === 'ios') ? getStatusBarHeight() : getAndroidStatusBarHeight();

// Header height
const HEADER_HEIGHT = (Platform.OS === 'ios') ? getStatusBarHeight() + 40 : 73;

export {
  STATUS_BAR_HEIGHT,
  HEADER_HEIGHT,
};
