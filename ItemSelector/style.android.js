import { StyleSheet, Dimensions } from 'react-native';

const style = StyleSheet.create({

  // Main container
  container: {},

  // Title
  titleContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 18,
    paddingBottom: 16,
  },
  titleText: {
    color: '#0000008a',
    fontFamily: 'Roboto-Regular',
    fontSize: 16,
  },

  // Item
  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 56,
    paddingLeft: 18,
  },
  itemText: {
    color: '#0000008a',
    fontFamily: 'Roboto-Regular',
    fontSize: 16,
    marginLeft: 34,
  },
});

export default style;
