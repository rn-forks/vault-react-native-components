import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Dimensions,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import I18n from 'react-native-i18n';

const screen = Dimensions.get('window');
/**
* Use this component in order to show a list of options.
*/
class ItemSelector extends Component {

  constructor(props, context) {
    super(props, context);

    this.renderButtons = this.renderButtons.bind(this);
    this.renderSeparator = this.renderSeparator.bind(this);
    this.onPressItem = this.onPressItem.bind(this);
  }

  // TODO: Remove onPressCancelButton, there is no reason to call that function here.
  onPressItem(rowData) {
    const { onItemSelect, onPressCancelButton } = this.props;

    onPressCancelButton();

    if (onItemSelect) onItemSelect(rowData);
  }

  renderButtons(rowData, sectionID, rowID) {
    const rowProps = {
      style: [localStyle.buttons],
      onPress: () => this.onPressItem(rowData),
    };
    const textStyle = [
      localStyle.btnText,
      { textAlign: 'center', marginLeft: 0 },
    ];

    return (
      <View key={rowID}>
        <TouchableOpacity {...rowProps}>
          <Text style={textStyle}>
            {rowData.name}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderSeparator(sectionID, rowID) {
    const { dataSource } = this.props;
    const dataSourceCount = dataSource && dataSource.length;
    const indexRow = parseInt(rowID, 10);

    if (indexRow === (dataSourceCount - 1)) return null;

    const separatorStyle = {
      borderBottomWidth: 1,
      borderColor: '#C1C1C1',
    };

    return (<View key={`${sectionID}-${rowID}`} style={separatorStyle} />);
  }

  renderTitle() {
    const { title } = this.props;

    if (!title) return null;

    const titleStyle = {
      borderBottomWidth: 1,
      borderColor: '#C1C1C1',
      backgroundColor: '#E5E5E5',
      borderTopLeftRadius: 9,
      borderTopRightRadius: 9,
      alignItems: 'center',
      height: 33,
      justifyContent: 'center',
    };

    return (
      <View style={titleStyle}>
        <Text style={localStyle.titleTxt}>{title}</Text>
      </View>
    );
  }

  renderCancelButton() {
    const { onPressCancelButton } = this.props;
    const cancelButtonProps = {
      onPress: onPressCancelButton,
      style: [
        localStyle.buttons,
        {
          alignItems: 'center',
          marginLeft: 10,
          backgroundColor: '#ffffffe6',
          borderRadius: 9,
          marginBottom: 10,
          marginTop: 8
        },
      ],
    };
    const textStyle = [
      localStyle.btnText,
      { fontSize: 20, color: '#777777', marginLeft: 0 },
    ];

    return (
      <TouchableOpacity {...cancelButtonProps}>
        <Text style={textStyle}>
          {I18n.t('global.cancel')}
        </Text>
      </TouchableOpacity>
    );
  }

  render() {
    const ds = this.props.dataSource;

    return (
      <View>
        <View style={localStyle.container}>
          {this.renderTitle()}
          <FlatList
            alwaysBounceVertical={false}
            dataSource={ds}
            renderRow={this.renderButtons}
            renderSeparator={this.renderSeparator}
          />
        </View>
        {this.renderCancelButton()}
      </View>
    );
  }
}

const localStyle = StyleSheet.create({
  btnText: {
    marginLeft: 16,
    fontSize: 20,
    letterSpacing: -0.4,
    color: '#C93D0A',
    fontFamily: 'Roboto-Regular',
  },
  container: {
    borderRadius: 9,
    width: screen.width - 20,
    marginLeft: 10,
    backgroundColor: '#ffffffe6',
  },
  buttons: {
    height: 57,
    width: screen.width - 20,
    justifyContent: 'center',
  },
  titleTxt: {
    fontSize: 12,
    lineHeight: 20,
    letterSpacing: 2,
    color: '#1A1A1A',
    fontFamily: 'Roboto-Regular',
    textAlign: 'center',
  },
});

ItemSelector.propTypes = {
  /** Array of objects, each item of the array has a 'name' required property. */
  dataSource: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
    })
  ).isRequired,
  /** The selector title. */
  title: PropTypes.string,
  /** On press cancel button callback. */
  onPressCancelButton: PropTypes.func.isRequired,
  /** Callback that is called when an item is selected. Parameter: 'item' selected. */
  onItemSelect: PropTypes.func.isRequired,
};

export default ItemSelector;
