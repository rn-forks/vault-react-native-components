import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import style from './style';

/**
* Use this component in order to show a list of options.
*/
class ItemSelector extends Component {

  constructor(props, context) {
    super(props, context);

    this.onPressItem = this.onPressItem.bind(this);
  }

  onPressItem(item) {
    const { onItemSelect, onPressCancelButton } = this.props;

    if (onPressCancelButton) onPressCancelButton();

    if (onItemSelect) onItemSelect(item);
  }

  // TODO: Customize the icons.
  renderIcon(item) {
    const iconProps = {
      name: 'rocket',
      size: 20,
      color: '#d55206',
    };

    return (<Icon  {...iconProps} />);

  }

  renderItem(item, index) {
    const itemProps = {
      key: index,
      style: style.itemContainer,
      onPress: () => { this.onPressItem(item); },
    };

    return (
      <TouchableOpacity {...itemProps}>
        {this.renderIcon(item)}
        <Text style={[style.itemText, this.props.textStyle]}>
          {item.name}
        </Text>
      </TouchableOpacity>
    );
  }

  renderTitle() {
    const { title } = this.props;

    if (!title) return null;

    return (
      <View style={[style.titleContainer, this.props.titleContainerStyle]}>
        <Text style={[style.titleText, this.props.titleTextStyle]}>{title}</Text>
      </View>
    );
  }

  render() {
    const { dataSource } = this.props;

    if (!dataSource) return null;

    return (
      <View style={style.container}>
        {this.renderTitle()}
        {dataSource.map(this.renderItem, this)}
      </View>
    );
  }
}

ItemSelector.propTypes = {
  /** Array of objects, each item of the array has a 'name' required property. */
  dataSource: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
    })
  ).isRequired,
  /** The selector title. */
  title: PropTypes.string,
  /** The style of the title container. */
  titleContainerStyle: PropTypes.object,
  /** The style of the title text. */
  titleTextStyle: PropTypes.object,
  /** The style of each item text. */
  textStyle: PropTypes.object,
  /** On press cancel button callback. */
  onPressCancelButton: PropTypes.func.isRequired,
  /** Callback that is called when an item is selected. Parameter: 'item' selected. */
  onItemSelect: PropTypes.func.isRequired,
};

export default ItemSelector;
