import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  TouchableWithoutFeedback,
  Animated,
  StyleSheet,
  Image,
  View,
  Easing
} from 'react-native';

import style from './style';

/**
* Use this component in order to rotate an image infinitely.
*/
class Whirl extends Component {

  constructor(props, context) {
    super(props, context);

    this.angleY = new Animated.Value(0);
    this.animY = Animated.timing(this.angleY, {
      toValue: 360,
      duration: 1500,
      easing: Easing.linear
    });
  }

  componentWillUnmount() {
    this.animY.stop();
  }

  componentDidMount() {
    this.animate();
  }

  animate() {
    this.angleY.setValue(0);

    Animated.sequence([ this.animY ]).start(() => this.animate());
  }

  getAnimatedImageStyles() {
    const { width, height } = this.props;
    const interpolateConfig = {
      inputRange: [0, 360],
      outputRange: ['0deg', '360deg'],
    };

    return [
      style.animatedImage,
      { width, height },
      { transform: [{ rotateY: this.angleY.interpolate(interpolateConfig) }] },
    ];
  }

  getAnimatedImageProps() {
    const { source } = this.props;

    return {
      source: source,
      style: this.getAnimatedImageStyles(),
    };
  }

  render() {
    const { source, width, height } = this.props;

    if (!source || !width || !height) return null;

    return (
      <View style={style.container}>
        <Animated.Image {...this.getAnimatedImageProps()}></Animated.Image>
      </View>
    );
  }
}

Whirl.propTypes = {
  /** A source object that Image component accepts. */
  source: PropTypes.object.isRequired,
  /** The image's width that you provided in source prop. */
  width: PropTypes.number.isRequired,
  /** The image's height that you provided in source prop. */
  height: PropTypes.number.isRequired,
};

export default Whirl;
