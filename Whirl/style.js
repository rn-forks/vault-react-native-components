import { StyleSheet, Platform } from 'react-native';
import { STATUS_BAR_HEIGHT } from '../utils/constants';

const style = StyleSheet.create({
  // Main container
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  // Animated image.
  animatedImage: {
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
});

export default style;
