import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TabBarIOS } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

class TabBar extends Component {

  state = {
    selectedItem: this.props.initialItem,
  }

  onPressTabBarItem = (icon) => (e) => {
    this.setState({ selectedItem: icon });
  }

  getTabBarProps() {
    const { barProps } = this.props;

    return barProps || {};
  }

  getTabBarItem(index) {
    const { icons } = this.props;
    const iconProps = icons[index];
    const iconName = iconProps && iconProps.iconName;
    const { selectedItem } = this.state;

    return {
      ...iconProps,
      renderAsOriginal: true,
      selected: (iconName === selectedItem),
      onPress: this.onPressTabBarItem(iconName),
    };
  }

  renderTabBarItem = (child, index) => {
    return (
      <Icon.TabBarItemIOS {...this.getTabBarItem(index)}>
        {child}
      </Icon.TabBarItemIOS>
    );
  }

  render() {
    const { children, icons } = this.props;
    if (!children || !icons || !icons.length) return null;

    const childrenCount = React.Children.count(children);
    if (childrenCount <= 0 || childrenCount > 5) return null;

    if (childrenCount !== icons.length) return null;

    return (
      <TabBarIOS {...this.getTabBarProps()}>
        {React.Children.map(children, this.renderTabBarItem)}
      </TabBarIOS>
    );
  }
}

TabBar.propTypes = {
  /**
      Each child correspond with the content. This component accepts children > 0 and
      children <= 5. If you need to show more options use OptionsListIOS component.
  */
  children: PropTypes.arrayOf(PropTypes.node.isRequired).isRequired,
  /** Array of objects. Each element of the array represents the props of the Icon.TabBarItemIOS component. */
  icons: PropTypes.array.isRequired,
  /** The initial item. */
  initialItem: PropTypes.string.isRequired,
  /** The TabBarIOS's props. */
  barProps: PropTypes.object,
};

export default TabBar;
