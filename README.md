# Vault - React Native components

Components set used by Vault to develop mobile apps.

## To use in your project

- Install: `npm install vault-react-native-components --save`.
- Ensure you have all required dependencies installed. (See peerDependencies in package.json)
- Some libraries needs to be linked, run `react-native link`.

## Linking

The following dependencies needs to be linked or configurated:

- react-native-vector-icons [Repo](https://github.com/oblador/react-native-vector-icons)

## Generating docs

[WIP] Important! This feature is work in progress, use it by your own risk.

```bash
npm run doc
```

Note: Make sure you have install [Vault Doc Generator](https://bitbucket.org/vaultdocsteam/vault-doc-generator/overview)

## Examples

See "Examples" folder.
