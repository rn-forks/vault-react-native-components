import { StyleSheet } from 'react-native';

const DEFAULT_DOT_RADIUS = 6;

const style = StyleSheet.create({
  // Main container
  container: {
    flexDirection: 'row',
    // TODO: Implement the correct height measure in order to support the layout on different devices.
    height: 60,
  },

  // Extra container
  containerDots: {
    flex: 2,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },

  // Dot
  dot: {
    width: DEFAULT_DOT_RADIUS,
    height: DEFAULT_DOT_RADIUS,
    borderRadius: DEFAULT_DOT_RADIUS >> 1,
    backgroundColor: '#BBBBBB',
    margin: DEFAULT_DOT_RADIUS >> 1
  },
  selectDot: {
    backgroundColor: 'white'
  },

  // Action buttons
  actionButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  previous: {
    flexDirection: 'row',
  },
  next: {
    flexDirection: 'row-reverse',
  },
});

export default style;
