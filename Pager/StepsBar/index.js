import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import { IndicatorViewPager } from 'rn-viewpager';
import IconApp from 'react-native-vector-icons/MaterialIcons';
import _ from 'lodash';

import style from './style';

class StepsBar extends Component {

  static defaultProps = {
    pageCount: 0,
    initialPage: 0,
    hideSingle: false
  }

  state = {
    selectedIndex: this.props.initialPage
  }

  constructor(props) {
    super(props);

    this.onPressPrevious = this.onPressPrevious.bind(this)
    this.onPressNext = this.onPressNext.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.state.selectedIndex != nextState.selectedIndex ||
    this.props.pageCount != nextProps.pageCount ||
    this.props.dotStyle != nextProps.dotStyle ||
    this.props.selectedDotStyle != nextProps.selectedDotStyle ||
    this.props.style != nextProps.style
  }

  renderActionButton(type) {
    const { previousAction, nextAction, disablePrevious, disableNext } = this.props;
    const handlers = { previous: this.onPressPrevious, next: this.onPressNext };
    const labels = { previous: previousAction.label, next: nextAction.label };
    const icons = { previous: previousAction.iconProps, next: nextAction.iconProps };
    const disableValues = { previous: disablePrevious, next: disableNext };
    const touchableProps = {
      onPress: handlers[type],
      style: [
        style.actionButton,
        style[type]
      ],
      disabled: disableValues[type],
    };

    return (
      <TouchableOpacity {...touchableProps}>
        {(icons[type]) ? <IconApp {...icons[type]} /> : null}
        <Text>{labels[type]}</Text>
      </TouchableOpacity>
    );
  }

  renderDots() {
    const { pageCount, dotStyle, selectedDotStyle } = this.props;
    const { selectedIndex } = this.state;
    let dotsView = [];

    for (let i = 0; i < pageCount; i++) {
      let isSelect = (i === selectedIndex);
      let newDotStyle = [
        style.dot,
        (isSelect) ? style.selectDot : null,
        (isSelect) ? selectedDotStyle : dotStyle,
      ];

      dotsView.push(<View key={i} style={newDotStyle} />);
    }

    return (
      <View style={[style.containerDots]}>{dotsView}</View>);
  }

  render() {
    const { pageCount, hideSingle, stepsBarStyle } = this.props;

    if (pageCount <= 0) return null;
    if (hideSingle && pageCount == 1) return null;

    return (
      <View style={[style.container, stepsBarStyle]}>
        {this.renderActionButton('previous')}
        {this.renderDots()}
        {this.renderActionButton('next')}
      </View>
    );
  }

  onPressPrevious() {
    const { pager } = this.props;
    const { selectedIndex } = this.state;
    const newIndex = selectedIndex - 1;

    if (this.isIndexInRange(newIndex)) pager.setPage(newIndex);
  }

  onPressNext() {
    const { pager } = this.props;
    const { selectedIndex } = this.state;
    const newIndex = selectedIndex + 1;

    if (this.isIndexInRange(newIndex)) pager.setPage(newIndex);
  }

  isIndexInRange(index) {
    const { pageCount } = this.props;

    return (
      index >= 0 &&
      index <= (pageCount - 1)
    );
  }

  onPageSelected(e) {
    this.setState({ selectedIndex: e.position }, this.notifyToParent.bind(this, e));
  }

  notifyToParent(e) {
    const { onPageSelected } = this.props;

    if (onPageSelected) onPageSelected(e);
  }
}

export default StepsBar;
