import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, ScrollView } from 'react-native';
import { IndicatorViewPager } from 'rn-viewpager';

import StepsBar from './StepsBar';
import style from './style';

/**
* Use this component in order to present a step by step flow to the user.
*/
class Pager extends Component {

  getIndicatorViewPagerProps() {
    return {
       style: style.indicator,
       horizontalScroll: false, // To work properly in Android
       scrollEnabled: false, // To work properly in iOS
       indicator: this.renderIndicator(),
    };
  }

  renderIndicator() {
    const {
      children,
      previousAction,
      nextAction,
      disablePrevious,
      disableNext,
      dotStyle,
      selectedDotStyle,
      stepsBarStyle,
      onPageSelected,
    } = this.props;
    const stepsBarProps = {
      pageCount: React.Children.count(children),
      disablePrevious,
      disableNext,
      previousAction,
      nextAction,
      dotStyle,
      selectedDotStyle,
      stepsBarStyle,
      onPageSelected,
    };

    return (<StepsBar {...stepsBarProps} />);
  }

  renderPage(page, index) {
    return (
      <View style={{ flex: 1 }} key={index}>
        {page}
      </View>
    )
  }

  renderPages() {
    const { children } = this.props;

    return React.Children.map(children, this.renderPage);
  }

  render() {
    return (
      <View style={style.container}>
        <IndicatorViewPager {...this.getIndicatorViewPagerProps()}>
          {this.renderPages()}
        </IndicatorViewPager>
      </View>
    );
  }
}

Pager.propTypes = {
  /** Each direct child will be a new page. */
  children: PropTypes.arrayOf(PropTypes.node.isRequired),
  /** Object configuration for previous action. */
  previousAction: PropTypes.shape({
    label: PropTypes.string.isRequired,
    iconProps: PropTypes.object,
  }),
  /** Object configuration for next action. */
  nextAction: PropTypes.shape({
    label: PropTypes.string.isRequired,
    iconProps: PropTypes.object,
  }),
  /** Determines if previous action is disabled. */
  disablePrevious: PropTypes.bool,
  /** Determines if next action is disabled. */
  disableNext: PropTypes.bool,
  /** Styles object for dots. */
  dotStyle: PropTypes.object,
  /** Styles object for dots when they are selected. */
  selectedDotStyle: PropTypes.object,
  /** Styles object for steps bar. */
  stepsBarStyle: PropTypes.object,
  /** Callback that is called when a page is shown. */
  onPageSelected: PropTypes.func,
};

export default Pager;
