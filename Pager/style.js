import { StyleSheet, Platform } from 'react-native';
import { STATUS_BAR_HEIGHT } from '../utils/constants';

const style = StyleSheet.create({
  // Main container
  container: {
    flex: 1,
    ...Platform.select({
      ios: {
        marginTop: STATUS_BAR_HEIGHT,
      }
    }),
  },

  // View Pager Indicator
  indicator: {
    flex: 1,
  }
});

export default style;
