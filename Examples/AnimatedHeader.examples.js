import React, { Component } from 'react';
import { Text, View, Animated } from 'react-native';
import { Header, AnimatedHeader, HeaderContent } from '../index';

class AnimatedHeaderExample extends Component {

  constructor(props, context) {
    super(props, context);

    this.onScrollListener = this.onScrollListener.bind(this);

    this.state = {
      scrollY: new Animated.Value(0),
    };
  }

  renderScrollViewContent() {
    const data = Array.from({ length: 30 });
    const styleRow = {
      height: 40,
      margin: 16,
      backgroundColor: '#D3D3D3',
      alignItems: 'center',
      justifyContent: 'center',
    };

    return data.map((obj, i) => (
      <View key={i} style={styleRow}>
        <Text>{i}</Text>
      </View>
    ));
  }

  onScrollListener(event, gesture) {
    const { scrollY } = this.state;

    scrollY.setValue(event.nativeEvent.contentOffset.y);

    this.setState({ scrollY });
  }

  renderTopNode() {
    const { scrollY } = this.state;
    const contentOpacity = scrollY.interpolate({
      inputRange: [0, 240],
      outputRange: [0, 1],
      extrapolate: 'clamp',
    });
    const title = { color: 'black', fontSize: 18, opacity: contentOpacity };

    return (
      <HeaderContent
        leftNode={<Text>{'Atras'}</Text>}
        middleNode={<Animated.Text style={title}>Title</Animated.Text>}
        rightNode={<Text>{'Actions'}</Text>}
      />
    );
  }

  renderBodyNode() {
    const title = { color: 'white', fontSize: 18 };

    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text style={title}>Title</Text>
      </View>
    );
  }

  render() {
    return (
      <AnimatedHeader
        imgSource={{ uri: 'https://static.pexels.com/photos/170811/pexels-photo-170811.jpeg'}}
        topNode={this.renderTopNode()}
        bodyNode={this.renderBodyNode()}
        onScrollListener={this.onScrollListener}
      >
        {this.renderScrollViewContent()}
      </AnimatedHeader>
    );
  }
}

export default AnimatedHeaderExample;
