import React from 'react';
import { Text, View } from 'react-native';
import { Header, HeaderContent } from '../index';

const HeaderWithTitle = (props) => {
  return (
    <Header>
      <HeaderContent middleNode={<Text>{'Title'}</Text>} />
    </Header>
  );
};

const HeaderWithBackButton = (props) => {
  return (
    <Header>
      <HeaderContent leftNode={<Text>{'Back'}</Text>} />
    </Header>
  );
};

const HeaderWithActions = (props) => {
  return (
    <Header>
      <HeaderContent rightNode={<Text>{'Actions'}</Text>} />
    </Header>
  );
};

export {
  HeaderWithTitle,
  HeaderWithBackButton,
  HeaderWithActions,
};
