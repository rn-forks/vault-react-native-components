import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Animated,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';

import style, { HEADER_SCROLL_DISTANCE } from './style';

// TODO: Use constants for header spacing.
/**
* This component renders an animated image at the top of the screen. The image collapse when the user scroll the content.
*/
class AnimatedHeader extends Component {
  constructor(props) {
    super(props);

    this.onScrollListener = this.onScrollListener.bind(this);

    this.state = {
      scrollY: new Animated.Value(0),
    };
  }

  getStatusBarProps() {
    return {
      translucent: true,
      barStyle: 'light-content',
      backgroundColor: 'rgba(0, 0, 0, 0.251)',
    };
  }

  getAnimatedScrollViewContent() {
    const { scrollY } = this.state;
    const event = [{ nativeEvent: { contentOffset: { y: scrollY } } }];
    const options = {
      useNativeDriver: true,
      listener: this.onScrollListener,
    };

    return {
      style: style.container,
      scrollEventThrottle: 1,
      onScroll: Animated.event(event, options),
    };
  }

  getAnimatedViewHeaderStyles() {
    const headerTranslate = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, -HEADER_SCROLL_DISTANCE],
      extrapolate: 'clamp',
    });

    return [
      style.header,
      { transform: [{ translateY: headerTranslate }] }
    ];
  }

  getAnimatedImageProps(imageBlur) {
    const { imgSource } = this.props;
    const { scrollY } = this.state;
    const isBlurred = (imageBlur === 10);
    const imageOpacity = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, (isBlurred) ? 0.5 : 1, (isBlurred) ? 1 : 0],
      extrapolate: 'clamp',
    });
    const imageTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, 100],
      extrapolate: 'clamp',
    });

    return {
      blurRadius: imageBlur,
      source: imgSource,
      style: [
        style.backgroundImage,
        {
          opacity: imageOpacity,
          transform: [{ translateY: imageTranslate }],
        },
      ],
    };
  }

  getAnimatedViewContentStyles() {
    const { scrollY } = this.state;
    const contentScale = scrollY.interpolate({
      inputRange: [0,  HEADER_SCROLL_DISTANCE],
      outputRange: [1, 0],
      extrapolate: 'clamp',
    });
    const contentOpacity = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 0],
      extrapolate: 'clamp',
    });

    return [
      style.bar,
      {
        opacity: contentOpacity,
        transform: [
          { scale: contentScale },
        ],
      },
    ];
  }

  onScrollListener(event, gestureState) {
    const { onScrollListener } = this.props;

    if (onScrollListener) onScrollListener(event, gestureState);
  }

  renderScrollViewContent() {
    const { children } = this.props;

    return (
      <View style={style.scrollViewContent}>
        {children}
      </View>
    );
  }

  render() {
    const { topNode, bodyNode } = this.props;

    return (
      <View style={style.container}>
        <StatusBar {...this.getStatusBarProps()} />
        <Animated.ScrollView {...this.getAnimatedScrollViewContent()}>
          {this.renderScrollViewContent()}
        </Animated.ScrollView>
        <Animated.View style={this.getAnimatedViewHeaderStyles()}>
          <Animated.Image {...this.getAnimatedImageProps(10)} />
          <Animated.Image {...this.getAnimatedImageProps(0)} />
          <Animated.View style={this.getAnimatedViewContentStyles()}>
            {bodyNode}
          </Animated.View>
        </Animated.View>
        <View style={style.topContent}>
          {topNode}
        </View>
      </View>
    );
  }
}

AnimatedHeader.propTypes = {
  /** The scroll view content. */
  children: PropTypes.node.isRequired,
  /** A source object that Image component accepts. */
  imgSource: PropTypes.object.isRequired,
  /** The node that you need to render at the top. Use this to render back button, actions, title, etc. */
  topNode: PropTypes.node,
  /** The node that you need to render in the middle of the image. */
  bodyNode: PropTypes.node,
  /** A callback that called when the user scroll the content. Receives one parameter: 'event'. */
  onScrollListener: PropTypes.func,
};

export default AnimatedHeader;
