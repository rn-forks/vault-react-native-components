import { StyleSheet } from 'react-native';
import { STATUS_BAR_HEIGHT, HEADER_HEIGHT } from '../utils/constants';

const style = StyleSheet.create({
  // Main container
  container: {
    flex: 1,
    flexDirection: 'row',
  },

  // Nodes
  left: {
    flex: 1,
    justifyContent: 'center',
  },
  middle: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  right: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
});

export default style;
