import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import style from './style';

/**
* You can use this component in order to show three columns and renders nodes on them.
*/
class HeaderContent extends Component {

  renderLeftNode() {
    const { leftNode } = this.props;

    return (<View style={style.left}>{leftNode}</View>);
  }

  renderMiddleNode() {
    const { middleNode } = this.props;

    return (<View style={style.middle}>{middleNode}</View>);
  }

  renderRightNode() {
    const { rightNode } = this.props;

    return (<View style={style.right}>{rightNode}</View>);
  }

  render() {
    const { leftNode, middleNode, rightNode } = this.props;

    if (!leftNode && !middleNode && !rightNode) return null;

    return (
      <View style={style.container}>
        {this.renderLeftNode()}
        {this.renderMiddleNode()}
        {this.renderRightNode()}
      </View>
    );
  }
}

HeaderContent.propTypes = {
  /** The component that will be displyed in the left of the screen. */
  leftNode: PropTypes.node,
  /** The component that will be displyed in the middle of the screen. */
  middleNode: PropTypes.node,
  /** The component that will be displyed in the right of the screen. */
  rightNode: PropTypes.node,
};

export default HeaderContent;
