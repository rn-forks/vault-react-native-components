import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import Menu, { MenuOptions, MenuOption, MenuTrigger } from 'react-native-menu';

import Icon from 'react-native-vector-icons/FontAwesome';

/*
+ Implementation Example
* <ContextMenu {...this.getContextMenuProps()} />
*
* getContextMenuProps() {
    return {
      options: this.getContextMenuOptions(),
      onSelectMenuItem: null, // there should be a handler to manage that action
      sizeIconAction: L.hProm(16),
      colorIconAction: 'red',
      nameIconAction: 'rocket',
      styleAction: localStyle.triggerAction,
    };
  }

  getContextMenuOptions() {
    const BUTTONS = {
      moveToDone: {
        icon: {
          name: 'rocket',
          size: L.hProm(18),
          color: Colors.black_54,
        },
        label: I18n.t('trips.moveToDone'),
        value: 'moveToDone',
      },
      remove: {
        icon: {
          name: 'rocket',
          size: L.hProm(18),
          color: Colors.black_54,
        },
        label: I18n.t('trips.delete'),
        value: 'remove',
      },
    };
    const options = [];

    options.push(BUTTONS.moveToDone);
    options.push(BUTTONS.remove);

    return options;
  }
*
* */

import style from './style';

/**
* Use this component in order to show a list of options in a Menu.
* This component uses [react-native-menu](https://github.com/jaysoo/react-native-menu), you need to configure
* the MenuContext component in your app, see the plugin's doc.
*/
class ContextMenu extends Component {

  constructor(props, context) {
    super(props, context);

    this.onMenuSelect = this.onMenuSelect.bind(this);
  }

  render() {
    const { options } = this.props;

    if (!options) return null;

    return (
      <View style={style.container}>
        <Menu {...this.getMenuProps()}>
          {this.renderTrigger()}
          {this.renderOptions()}
        </Menu>
      </View>
    );
  }

  renderTrigger() {
    const { styleAction } = this.props;
    const triggerProps = {
      renderTouchable: () => <TouchableOpacity />,
      style: [style.trigger, styleAction],
    };

    return (
      <MenuTrigger {...triggerProps}>
        <Icon {...this.getActionIconProps()} />
      </MenuTrigger>
    );
  }

  renderOptions() {
    const { options } = this.props;

    return (
      <MenuOptions style={style.menuOptions}>
        {options.map(this.renderItem, this)}
      </MenuOptions>
    );
  }

  renderItem(item, index) {
    const { icon, label } = item;
    const itemProps = {
      value: index,
      style: style.option,
      renderTouchable: () => <TouchableOpacity />,
    };

    return (
      <MenuOption key={index} {...itemProps}>
        <Text style={style.optionText}>{label}</Text>
      </MenuOption>
    );
  }

  onMenuSelect(indexItemSelected) {
    const { onSelectMenuItem, options } = this.props;
    const optionSelected = options[indexItemSelected].value;

    if (onSelectMenuItem) onSelectMenuItem(optionSelected);
  }

  getMenuProps() {
    return {
      onSelect: this.onMenuSelect,
      style: style.menu,
    };
  }

  getActionIconProps() {
    const { sizeIconAction, colorIconAction, nameIconAction } = this.props;

    return {
      name: nameIconAction,
      size: sizeIconAction,
      color: colorIconAction,
      style: style.icon,
    };
  }
}

ContextMenu.propTypes = {
  /** Array of objects. */
  options: PropTypes.arrayOf(
    PropTypes.shape({
      /** Menu item icon. */
      icon: PropTypes.shape({
        /** Icon name. */
        name: PropTypes.string,
        /** Icon size. */
        size: PropTypes.number,
        /** Icon color. */
        color: PropTypes.string,
      }),
      /** Menu item label. */
      label: PropTypes.string,
      /** Menu item value. This value is used by onSelectMenuItem callback */
      value: PropTypes.any.isRequired,
    }),
  ),
  /** The style of the action trigger. */
  styleAction: PropTypes.object,
  /** The icon size of the action trigger. */
  sizeIconAction: PropTypes.number,
  /** The icon color of the action trigger. */
  colorIconAction: PropTypes.string,
  /** The icon name of the action trigger. */
  nameIconAction: PropTypes.string,
  /** Callback that is called when a menu option is selected. Parameter: 'value'. */
  onSelectMenuItem: PropTypes.func.isRequired,
};

export default ContextMenu;
