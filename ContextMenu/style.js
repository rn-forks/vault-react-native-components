import {
  StyleSheet,
  Platform,
} from 'react-native';

const style = StyleSheet.create({

  // Main container
  container: {},
  // Menu
  menu: {},
  // Menu Options
  menuOptions: {},
  // Menu trigger
  trigger: {
    ...Platform.select({
      ios: {
        paddingTop: 14,
        paddingBottom: 4,
        paddingLeft: 9,
        paddingRight: 13,
      },
      android: {
        paddingTop: 14,
        paddingBottom: 4,
        paddingLeft: 9,
        paddingRight: 9,
      },
    }),
  },
  // Trigger Icon
  icon: {},
  option:{
  },
  // Menu Option item
  option: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingVertical: 13,
  },

  // Option Icon
  optionIcon: {
    marginRight: 17,
  },

  // Option Text
  optionText: {
    ...Platform.select({
      ios: {
        color: '#d55206',
        fontSize:17,
        fontFamily: 'Roboto-Regular',
      },
      android: {
        color: '#0000008a',
        fontSize: 16,
        fontFamily: 'Roboto-Regular',
      },
    }),
  },
});

export default style;
